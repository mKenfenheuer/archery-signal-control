﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Media;
using System.IO;
namespace Tcp_Client
{
    public partial class Form1 : Form
    {
        SoundPlayer player = new SoundPlayer(Ampelsteuerung.Resource1.beep);
        static bool shooting = false;
        static bool send = true;
        static double time = 0;
        static string mode = "00";
        static int ab = 0;
        // The port number for the remote device.
        private const int port = 8888;
        List<Thread> threads = new List<Thread>();
        Thread timer = null;
        Thread remoteT = null;
        static Color color = Color.FromArgb(0, 255, 170, 0);
        void timeThread()
        {
            DateTime last = DateTime.Now;
            while (send)
            {
                last = DateTime.Now;
                if (time > 0.2)
                {
                    Thread.Sleep(10);
                    TimeSpan ts = DateTime.Now - last;
                    time -= (double)ts.Milliseconds / 1000.0;
                    last.Add(ts);
                }
            }
        }
        Form1 frm;
        void remote()
        {
            while (send)
            {
                try
                {
                    TcpListener l = new TcpListener(8099);
                    l.Start();
                    while (send)
                    {
                        if (l.Pending())
                        {
                            TcpClient c = l.AcceptTcpClient();
                            StreamReader r = new StreamReader(c.GetStream());
                            string s = r.ReadLine();
                            s = s.Substring(4);
                            s = s.Substring(0, s.IndexOf("HTTP")).Trim();
                            if (s != "/")
                            {
                                switch (s.Replace('/', ' ').Trim())
                                {
                                    case "up":
                                        frm.Invoke((MethodInvoker)(delegate ()
                                         {
                                             if (time == -1)
                                                 Start();
                                         }));
                                        break;
                                    case "select":
                                        frm.Invoke((MethodInvoker)(delegate ()
                                        {
                                            if (time == -1)
                                                ab = 1 - ab;
                                        }));
                                        break;
                                    case "down":
                                        frm.Invoke((MethodInvoker)(delegate ()
                                        {
                                            if (time > -1)
                                                Stop();
                                        }));
                                        break;
                                }
                            }
                            c.Close();
                        }
                        else
                        {
                            Thread.Sleep(1000);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
        int keepalive = 100;
        void Sender(object ip)
        {
            int timeKeep = 0;
            while (send)
            {
                timeKeep = 0;
                try
                {

                    String last = "";
                    TcpClient c = new TcpClient();
                    c.Connect((string)ip, port);
                    try
                    {

                        string sending = "";
                        Send(c, "STP255255255000");
                        while (send)
                        {
                            //timeKeep = 0;
                            if (time <= -1 && time > -2)
                            {
                                sending = "STP255  0  0";
                                //ab = 1 - ab;
                            }
                            else if (time == 0)
                            {
                                sending = "STP255  0  0";
                            }
                            else
                            {
                                if (!shooting)
                                {
                                    sending = maximize(time) + "255  0  0";
                                }
                                else
                                {
                                    if (Math.Round(time, 0) < 31)
                                    {
                                        sending = maximize(time) + maximize(color.R) + maximize(color.G) + maximize(color.B);
                                    }
                                    else
                                    {
                                        sending = maximize(time) + "  0255  0";
                                    }
                                }
                            }


                            sending += mode + ab;
                            if (last != sending)
                            {
                                //Console.WriteLine("Update: " + ip);
                                Send(c, sending);
                            }
                            last = sending;
                            timeKeep += 10;
                            if (timeKeep > keepalive)
                            {
                                timeKeep = 0;
                                //Console.WriteLine("Keepalive: " + ip);
                                Send(c, sending);
                            }
                            Thread.Sleep(10);
                        }

                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.ToString());
                        c.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

        }
        static int liga = 0;
        private static void Send(TcpClient c, String data)
        {
            StreamReader r = new StreamReader(c.GetStream(), Encoding.ASCII);
            r.BaseStream.ReadTimeout = 700;
            StreamWriter w = new StreamWriter(c.GetStream(), Encoding.ASCII);
            w.AutoFlush = true;
            w.Write(data + liga);
            char[] bff = new char[16];
            string read = r.ReadBlock(bff, 0, 16).ToString();
            StringBuilder build = new StringBuilder();
            build.Append(bff);
            if (build.ToString() != data + liga)
                throw new TimeoutException();
        }

        public static string[] pings = new string[0];
        List<Thread> pingers = new List<Thread>();
        public Form1(string[] ips)
        {
            frm = this;
            remoteT = new Thread(new ThreadStart(remote));
            remoteT.Start();
            timer = new Thread(new ThreadStart(timeThread));
            timer.Start();
            pings = new string[ips.Length];
            int count = 0;
            foreach (string ip in ips)
            {
                pings[count] = ip;
                Thread p = new Thread(new ParameterizedThreadStart(ping));
                p.Start(count);
                Thread t = new Thread(new ParameterizedThreadStart(Sender));
                t.Start(ip);
                threads.Add(t);
                count++;
            }
            InitializeComponent();


            // Receive the response from the remote device.
            //Receive(client);
            //receiveDone.WaitOne();

            // Write the response to the console.
            //Console.WriteLine("Response received : {0}", response);
        }
        public void ping(object ip)
        {
            string ipaddr = pings[(int)ip];
            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            while (send)
            {
                try
                {
                    System.Net.NetworkInformation.PingReply pr = ping.Send(ipaddr);
                    if (pr.Status == System.Net.NetworkInformation.IPStatus.Success)
                    {
                        pings[(int)ip] = ipaddr + " " + pr.RoundtripTime + "ms";
                        Thread.Sleep(100);
                    }
                    else
                    {
                        pings[(int)ip] = ipaddr + " TIMEOUT ";
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Start();
        }
        static string maximize(double input)
        {
            string s = "";
            if (input < 999)
            {
                s = Math.Round(input, 0) + "";
                while (s.Length < 3)
                    s = " " + s;
            }
            return s;
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            send = false;
        }
        int times = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            //time -= 0.1;
            if (Math.Round(time, 0) < 1)
            {

                radioButton1.Checked = ab == 0;
                radioButton2.Checked = ab == 1;
                if (shooting)
                {

                    timer1.Enabled = false;
                    label1.BackColor = Color.Red;
                    if (times == 0 && liga != 1 && !btnBenutzer.Checked)
                    {
                        ab = 1 - ab;
                        beeps = 2;
                        Start();
                    }
                    else
                    {
                        groupBox5.Enabled = true;
                        groupBox4.Enabled = true;
                        groupBox3.Enabled = true;
                        times = -1;
                        time = -1;
                        beeps = 3;
                    }
                    times++;

                    shooting = false;

                }
                else
                {
                    beeps = 1;
                    if (btnHalle.Checked)
                    {
                        time = 120.5;
                    }
                    if (btnLiga.Checked)
                    {
                        time = 120.5;
                    }
                    if (btnFita.Checked)
                    {
                        time = 240.5;
                    }
                    if (btnBenutzer.Checked)
                    {
                        time = (int)timeGreen.Value + 0.5;
                    }
                    shooting = true;
                    label1.BackColor = Color.Green;
                    label5.BackColor = Color.Green;
                }
            }
            if (ab == 0)
            {
                label5.Text = "A\r\n\r\n\r\nB";
            }
            else
            {
                label5.Text = "C\r\n\r\n\r\nD";
            }
            if (shooting && Math.Round(time, 0) < 11)
            {
                label1.BackColor = Color.Yellow;
                label5.BackColor = Color.Yellow;
            }
            if (!shooting && Math.Round(time, 0) < 31)
            {
                label1.BackColor = Color.Red;
                label5.BackColor = Color.Red;
            }
            if (time < 0)
            {
                label1.Text = "STOP";
            }
            else
            {
                label1.Text = Math.Round(time, 0) + "";
            }
            label6.Text = "";
            foreach (string s in (string[])pings.Clone())
            {
                label6.Text += s + "\n";
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                Start();
            }
            else if (e.KeyCode == Keys.F2)
            {
                Stop();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Stop();
                beeps = 5;
            }
        }
        void Start()
        {
            groupBox5.Enabled = false;
            groupBox4.Enabled = false;
            groupBox3.Enabled = false;
            beeps = 2;
            if (btnHalle.Checked)
            {
                time = 10.5;
            }
            if (btnLiga.Checked)
            {
                time = 10.5;
            }
            if (btnFita.Checked)
            {
                time = 10.5;
            }
            if (btnBenutzer.Checked)
            {
                time = (int)timeRed.Value + 0.5;
            }
            timer1.Enabled = true;
            send = true;
            if (time < 0)
            {
                label1.Text = "STOP";
            }
            else
            {
                label1.Text = Math.Round(time, 0) + "";
            }
        }
        void Stop()
        {
            groupBox5.Enabled = true;
            groupBox4.Enabled = true;
            groupBox3.Enabled = true;
            shooting = true;
            time = 0;
            send = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            mode = comboBox1.SelectedIndex + "0";
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            mode = comboBox1.SelectedIndex + "0";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                ab = 0;
            }
            else
            {
                ab = 1;
            }
            //Send(client, "STP255  0  000" + ab);
            if (ab == 0)
            {
                label5.Text = "A\r\n\r\n\r\nB";
            }
            else
            {
                label5.Text = "C\r\n\r\n\r\nD";
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                ab = 0;
            }
            else
            {
                ab = 1;
            }
            //Send(client, "STP255  0  000" + ab);
            if (ab == 0)
            {
                label5.Text = "A\r\n\r\n\r\nB";
            }
            else
            {
                label5.Text = "C\r\n\r\n\r\nD";
            }
        }
        int beeps = 0;
        private void timer2_Tick(object sender, EventArgs e)
        {
            player.Load();
            if (beeps > 0)
            {
                for (int i = 0; i < beeps; i++)
                {
                    player.Play();
                    Thread.Sleep(600);
                }
                beeps = 0;
            }
        }

        private void btnLiga_CheckedChanged(object sender, EventArgs e)
        {
            if (btnLiga.Checked)
                liga = 1;
            else
                liga = 0;

            label5.Visible = liga == 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void BtnBenutzer_CheckedChanged(object sender, EventArgs e)
        {
            if (btnBenutzer.Checked)
                liga = 1;
            else
                liga = 0;

            label5.Visible = liga == 0;
        }
    }

}
