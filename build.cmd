@echo OFF
setlocal
echo Restoring NuGet Packages...
@C:\GitLab-Runner\nuget.exe restore "Ampelsteuerung.sln">nul

echo Current Path is 
cd

@rmdir .\Build\ /s /q>nul
@md .\Build\>nul
@md .\Build\binaries>nul

echo Building
@C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe /consoleloggerparameters:ErrorsOnly /maxcpucount /nologo /property:Configuration=Release /verbosity:quiet "Ampelsteuerung.sln" >nul

echo Copying Build-Output
@robocopy .\Ampelsteuerung\bin\Release\ .\Build\binaries\ *.exe *.dll>nul
@robocopy .\Watchdog\bin\Release\ .\Build\ *.exe *.dll>nul

echo Done

endlocal 