﻿using System;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Net;
using System.IO;
using System.Linq;
using System.Net.Sockets;

namespace AmpelEmulator
{
    public class DataReader
    {
        public EventHandler<DataReceivedEventArgs> OnDataReceived;

        TcpListener _listener = new TcpListener(IPAddress.Any, 8888);

        public void Start()
        {
            _listener.Start();
            _listener.BeginAcceptTcpClient(new AsyncCallback(AcceptClient), _listener);
        }

        private void AcceptClient(IAsyncResult asyncResult)
        {
            TcpClient tcpClient = _listener.EndAcceptTcpClient(asyncResult);
            _listener.BeginAcceptTcpClient(new AsyncCallback(AcceptClient), _listener);
            Thread t = new Thread(new ParameterizedThreadStart(ProcessClient));
            t.IsBackground = true;
            t.Start(tcpClient);
        }

        private void ProcessClient(object param)
        {
            TcpClient tcpClient = (TcpClient) param;
            Stream str = tcpClient.GetStream();
            try
            {
                while (str.CanRead)
                {
                    char[] data = ReadChars(str, 16);
                    str.Write(Encoding.ASCII.GetBytes(data), 0, Encoding.ASCII.GetByteCount(data));
                    str.Flush();
                    Console.WriteLine(data);
                    OnDataReceived?.Invoke(this, new DataReceivedEventArgs(data));
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        private char[] ReadChars(Stream stream, int amount)
        {
            int charLength = Encoding.ASCII.GetByteCount(new char[] { 'c' });
            int read = 0;
            byte[] data = new byte[amount * charLength];
            while(read != amount * charLength)
            {
                read += stream.Read(data, read, amount * charLength - read);
            }

            return Encoding.ASCII.GetChars(data);
        }

        public class DataReceivedEventArgs : EventArgs
        {
            public DataPart Data { get; private set; }

            public DataReceivedEventArgs(char[] data)
            {
                Data = new DataPart(data);
            }
        }

        public class DataPart
        {
            public string TimeData { get; private set; }
            public Color TimeColor { get; private set; }
            public bool ColoredTime { get; private set; }
            public bool ABDisplay { get; private set; }
            public bool ABHide { get; private set; }

            public DataPart(char[] data)
            {
                TimeData = new string (data.Take(3).ToArray());
                int R = int.Parse(new string(data.Skip(3).Take(3).ToArray()));
                int G = int.Parse(new string(data.Skip(6).Take(3).ToArray()));
                int B = int.Parse(new string(data.Skip(9).Take(3).ToArray()));
                TimeColor = Color.FromArgb(R, G, B);
                ColoredTime = data[12] == '0';
                ABDisplay = data[14] == '0';
                ABHide = data[15] == '1';
            }
        }
    }
}
