﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmpelEmulator
{
    public partial class Form1 : Form
    {
        DataReader _reader = new DataReader();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _reader.OnDataReceived += OnDataReceived;
            _reader.Start();
        }

        private void OnDataReceived(object sender, DataReader.DataReceivedEventArgs e)
        {
            this.Invoke((MethodInvoker) (() =>
            {
                DataReader.DataPart dataPart = e.Data;
                abDisplay.Visible = !dataPart.ABHide;
                abDisplay.Text = dataPart.ABDisplay ? "AB" : "CD";
                this.BackColor = Color.Black;
                timeDisplay.Text = dataPart.TimeData == "STP" ? "STOP " : dataPart.TimeData;
                timeDisplay.ForeColor = dataPart.ColoredTime ? dataPart.TimeColor : Color.White;
            }));
        }

        private void TimeDisplay_Click(object sender, EventArgs e)
        {

        }
    }
}
