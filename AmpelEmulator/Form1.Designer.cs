﻿namespace AmpelEmulator
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.timeDisplay = new System.Windows.Forms.Label();
            this.abDisplay = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timeDisplay
            // 
            this.timeDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 128F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeDisplay.ForeColor = System.Drawing.Color.White;
            this.timeDisplay.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.timeDisplay.Location = new System.Drawing.Point(235, 9);
            this.timeDisplay.Name = "timeDisplay";
            this.timeDisplay.Size = new System.Drawing.Size(553, 432);
            this.timeDisplay.TabIndex = 0;
            this.timeDisplay.Text = "000";
            this.timeDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.timeDisplay.Click += new System.EventHandler(this.TimeDisplay_Click);
            // 
            // abDisplay
            // 
            this.abDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 128F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abDisplay.ForeColor = System.Drawing.Color.White;
            this.abDisplay.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.abDisplay.Location = new System.Drawing.Point(12, 9);
            this.abDisplay.Name = "abDisplay";
            this.abDisplay.Size = new System.Drawing.Size(217, 432);
            this.abDisplay.TabIndex = 1;
            this.abDisplay.Text = "A\r\nB\r\n";
            this.abDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.abDisplay);
            this.Controls.Add(this.timeDisplay);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "AmpelEmulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label timeDisplay;
        private System.Windows.Forms.Label abDisplay;
    }
}

